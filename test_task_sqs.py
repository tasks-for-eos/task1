#!/usr/bin/env python
# -*- coding: utf-8 -*-


import boto3
import json
import os

sqs_url = 'https://sqs.us-west-2.amazonaws.com/485190939626/SimpleQ'
region_name = 'us-west-2'
queue_name = 'SimpleQ'
max_queue_messages = 10
message_bodies = []
aws_access_key_id = '' # need to add aws credentials "Access Key"
aws_secret_access_key = '' # need to add aws credentials "Secret Access Key"

client = boto3.client('sqs', region_name="us-west-2", aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

while True:
    messages = client.receive_message(QueueUrl=sqs_url,MaxNumberOfMessages=max_queue_messages) 
    if 'Messages' in messages: 
        for message in messages['Messages']:             
            # print(message['Body'])
            message_bodies.append(message['Body'])
                        
    else:
        # print('Empty queue')
        break
# print(message_bodies)

if len(message_bodies) !=0:
    for message in message_bodies:
        print(message)
        os.system ("bash -c 'sleep 20'")
else:
    print("Empty queue")






